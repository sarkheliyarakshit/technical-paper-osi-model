
# OSI Model

## Introduction

OSI model full form is an Open system interconnection model.
 It consists of seven layers and each layer performs a particular network task.
 It is used to describes how the data is a transferred from one computer to another computer.
 OSI model introduces by the international standardization organization.
 It divides the whole task into seven smaller tasks. Each layer is assigned a particular task.

## OSI Layers functions

There are seven OSI layers. Each layer has different functions.

1. Application layer protocols
2. Presentation layer
3. Session layer
4. Transport layer
5. Network layer
6. Datalink layer
7. Physical layer

### Application layer

Network Application are the applications that use the internet like chrome, firefox, outlook, skype, etc.
The web browser is a network Application that running on a computer. It uses Application
layer protocols HTTP, HTTPS, FTP, etc. These protocols form the basis of File Transfer,
Web Surfing, Emails, Virtual Terminals, etc. File Transfer is done by FTP protocols,
Web Surfing is done by HTTP/s protocols, Emails is done by SMTP, Virtual Terminals
is done by Telnet.

### Presentation layer

That received data from the Presentation layer. This data is form numbers and
characters. This layer is converted to a machine-understandable binary format.
Then reduce the size of data using data-Compression. It can be Lossy or Lossless.
Data Compression is very helpful in real-time video and audio streaming.
To maintain security data can be encrypt by the sender side and the receiver side decrypt.
That can do by SSL(Secure Sockets Layer).

### Session layer

It helps and setting up and managing connections just before the connection is established with the server. The server performs the Authentication function. Authentication
is to verify the user. Authorization is to check whether the user has permission to access that file.
If not then it shows the message that you are not authorized to access this page.

### Transport layer

This layer control reliability of communication through Segmentation, Flow control, and Error control. In the Segmentation data receive from the session layer is divided
into a small data unit called segments. Each segment contains the source and destination
port number and sequence number. Port number helps to direct each segment to the correct
application. A sequence number is used for reassembled segments in to correct order.
Flow control amount of data being transmitted. Error control is used for some data
that does not arrive at the destination automatic repeat request to send again data.
That provide two types of service connection-oriented Transmission and connectionless
transmission. Connection-oriented Transmission done by TCP and connectionless
transmission UDP.

### Network Layer

This layer has three functions logical addressing, Routing, Path determination, IP address done in Logical
addressing every computer in the network has a unique IP address. Routing is a method to moving data packets from source to destination and it is base on the logical address format of Ipv4 and Ipv6. Network in many paths for
delivering a package from source to destination. Choosing the best possible path for data delivering from source
to destination is called path determination. Using those protocols OSPF, BGP, IS-IS to get the best path for delivering
data.

### Datalink layer

This data link layer receives the package from the network layer. There are two kinds of addresses. Logical addressing and Physical addressing. Logical addressing is done at the network layer. Physical addressing is done at the Datalink
layer in the sender and receiver mac address assigned to each data packet to form the frame. That provides media access and also controls how data is placed and received from the media. Datalink layer with its media access
method control data transmission. The tail of each contain bits which are used to detect the error in the received frame.

### Physical layer

It receives data sequence of binary zero and one. This layer converts binary layer sequence to signals and transmitted our local media.

## Document reference

This video for OSI model
[link](https://www.youtube.com/watch?v=vv4y_uOneC0)
